import pandas
import scipy
from sklearn.metrics import cohen_kappa_score
import random
import numpy

random.seed(42)
dfm = pandas.read_csv('./resultsmtcnn.csv')
dfo = pandas.read_csv('./resultsopencv.csv')
dfo2 = pandas.read_csv('./resultsopencv2.csv')
dfr = pandas.read_csv('./resultsretinaface.csv')
dfr2 = pandas.read_csv('./resultsretinaface2.csv')
dfs = pandas.read_csv('./resultsssd.csv')
dfautosamp = pandas.read_csv('./sampleautomatic.csv')
dfmanualsamp = pandas.read_csv('./samplemanual.csv')

print(dfr.loc[dfr.Name == 'Weidel','Emotion'].value_counts())
#print(dfo.loc[dfo.Emotion == 'not identified',:])
#List1 = dfm.index[dfm.Emotion == 'not identified'].to_list()
#List2 = dfr.index[dfr.Emotion == 'neutral'].to_list()
#print(len([x for x in List1 if x in List2]))
#Populist = [157, 136, 278, 72]
#Non_Populist = [336, 299, 125, 110, 191]
#print(scipy.stats.ttest_ind(a=Populist, b=Non_Populist, alternative= 'greater', equal_var=False))
#Populist = [157, 278]
#print(scipy.stats.ttest_ind(a=Populist, b=Non_Populist, alternative= 'greater', equal_var=False)) #Linke Hypothesis test
#Populist = [136, 72]
#print(scipy.stats.ttest_ind(a=Populist, b=Non_Populist, alternative= 'greater', equal_var=False)) # Afd Hypothesis test
#print(dfr.compare(dfr2))
#print(dfo.compare(dfo3))

#samplenumbers = numpy.sort(numpy.concatenate([random.sample(range(x, 1250+x), 10) for x in range(0, 10001, 1250)], axis=0))
#dfr.iloc[samplenumbers].to_csv('./sampleautomatic.csv', encoding='utf-8', index_label='index')
autosamp = dfautosamp.loc[:,'Emotion'].to_list()
manulsamp = dfmanualsamp.loc[:,'Emotion'].to_list()
print(cohen_kappa_score(autosamp, manulsamp))