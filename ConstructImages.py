import cv2
import glob
import os


if not os.path.exists('./imagedata'):
    os.mkdir('./imagedata')
paths = glob.glob('./data/*.mp4')
for path in paths:
    videopath = path.split(os.sep)[-1]
    videopath = videopath.replace('.mp4','')
    video = cv2.VideoCapture(path)
    framenr = 0
    while(True):
        frameexists, frame = video.read()
        if frameexists:
            cv2.imwrite(f'./imagedata/{videopath}-{framenr}.png', frame)
        else:
            break
        framenr += 1
    video.release()


