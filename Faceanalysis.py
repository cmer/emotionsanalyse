from deepface import DeepFace
import csv
import glob
import os

with open('resultsretinaface2.csv', 'w', newline='') as file:
    csvwriter = csv.writer(file)
    csvwriter.writerow(['Name', 'Partei','Geschlecht', 'Datum', 'Position', 'Emotion'])
    Lookup = {
            'Bartsch' : ['Linke', 'M'],
            'Chrupalla' : ['Afd', 'M'],
            'Droege' : ['Gruenen', 'W'],
            'Duerr' : ['Fdp', 'M'],
            'Haselmann' : ['Gruenen', 'W'],
            'Merz' : ['Cdu', 'M'],
            'Mohamed Ali' : ['Linke', 'W'],
            'Muetzenich' : ['Spd', 'M'],
            'Weidel' : ['Afd', 'W']
            }
    paths = glob.glob('./imagedata/*.png')
    for imagepath in paths:
        pathinfo = imagepath.split(os.sep)[-1]
        pathinfo = pathinfo.replace('.png', '')
        Info = pathinfo.split('-')
        Name = Info[0]
        Looked = Lookup[Name]
        try:
            Emotion = DeepFace.analyze(img_path=imagepath, actions=['emotion'], detector_backend='retinaface')
            csvwriter.writerow([Name, Looked[0], Looked[1], Info[1], Info[2], Emotion[0]['dominant_emotion']])

        except:
            csvwriter.writerow([Name, Looked[0], Looked[1], Info[1], Info[2], 'not identified'])
            continue
